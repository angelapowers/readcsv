package com.tienda.master.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.tienda.master.models.UseUp;


public interface UseUpRepository extends MongoRepository <UseUp,Integer> {

}
