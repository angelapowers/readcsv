package com.tienda.master.metods;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.opencsv.bean.CsvToBeanBuilder;
import com.tienda.master.models.CsvUse;
import com.tienda.master.models.SaleChannel;
import com.tienda.master.models.UseUp;
import com.tienda.master.repositories.UseUpRepository;

@Service
public class Expend {
	
	@Autowired
	UseUpRepository useUpRepository;
	
	public List<CsvUse> readAll() throws IOException {
		List<CsvUse> csvReaderUse = new CsvToBeanBuilder(new FileReader("C:\\Users\\GF63\\eclipse-workspace\\csv-1\\src\\main\\resources\\static\\Reporte info productos API_V2 Dev 30-03-2022.csv"))
		        .withType(CsvUse.class)
		        .build()
		        .parse();
		for(CsvUse dato: csvReaderUse) {
			SaleChannel reg = new SaleChannel(dato);
			UseUp registro = new UseUp(dato,reg);
			System.out.println(registro.toString());
			useUpRepository.save(registro);
		}
	return csvReaderUse;
	}
	
}
