package com.tienda.master.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tienda.master.metods.Expend;
import com.tienda.master.models.CsvUse;
import com.tienda.master.models.SaleChannel;
import com.tienda.master.models.UseUp;

@RestController
public class Controller {
	
	@Autowired
	Expend expend;
	
	@GetMapping("/lista")
	public List<CsvUse> getList() throws IOException{
		return expend.readAll();
	}

}
