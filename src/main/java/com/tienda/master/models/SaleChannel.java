package com.tienda.master.models;


import lombok.Data;

@Data
public class SaleChannel {
	private String channel_id;
	private Integer product_id; 
	
	public SaleChannel (CsvUse data) {
		this.channel_id =("SR");
		this.product_id =(data.getId_sears());
	}
}
