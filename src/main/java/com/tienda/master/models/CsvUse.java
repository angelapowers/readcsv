package com.tienda.master.models;

import com.opencsv.bean.CsvBindByName;

import lombok.Data;

@Data
public class CsvUse {
	@CsvBindByName(column = "Id_SEARS")
	private Integer id_sears;
	@CsvBindByName(column = "SKU")
	private Integer sku;
	@CsvBindByName(column = "Estatus")
	private String estatus;
	@CsvBindByName(column = "EAN")
	private Double ean;

}
