package com.tienda.master.models;

import org.springframework.data.mongodb.core.mapping.Document;


import lombok.Data;

@Document(collection = "useup")
@Data
public class UseUp {
	private Integer id; 
	private SaleChannel sale_channel;
	
	public UseUp (CsvUse data, SaleChannel channel) {
		this.id = data.getSku();
		this.sale_channel = channel;
	}
	
	public UseUp() {}
}
